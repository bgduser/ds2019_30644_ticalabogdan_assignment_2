package com.example.rabbit;

import java.io.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;


public class DataReader {
    private InputStream str=getClass().getClassLoader().getResourceAsStream("activity.txt");
    private  BufferedReader in = new BufferedReader(new InputStreamReader(str));

    public DataReader() {

    }

    public Message computeMessage() throws IOException {
        String read ;
        Message message;

        if ((read = in.readLine()) != null) {
            String[] splited = read.split("\\s{2,}");
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("uuuu-MM-dd HH:mm:ss");
            LocalDateTime startTime = LocalDateTime.parse(splited[0], formatter);
            LocalDateTime endTime = LocalDateTime.parse(splited[1], formatter);
            String activity = splited[2];
            message = new Message(startTime, endTime, activity);
            return message;
        } else {
            return null;
        }

    }
}
