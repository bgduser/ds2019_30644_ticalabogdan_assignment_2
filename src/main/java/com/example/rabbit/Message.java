package com.example.rabbit;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.time.LocalDateTime;

public class Message implements Serializable {


    private LocalDateTime startTime;
    private LocalDateTime endTime;
    private String activity;

    public Message() {

    }

    public Message(@JsonProperty("startTime")  LocalDateTime startTime,
                   @JsonProperty("endTime")  LocalDateTime endTime,
                   @JsonProperty("activity")  String activity) {
        this.startTime = startTime;
        this.endTime = endTime;
        this.activity = activity;
    }


    public LocalDateTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalDateTime startTime) {
        this.startTime = startTime;
    }

    public LocalDateTime getEndTime() {
        return endTime;
    }

    public void setEndTime(LocalDateTime endTime) {
        this.endTime = endTime;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    @Override
    public String toString() {
        return "[Start time = " + startTime + ", end time = " + endTime + ", activity = " + activity + "]";
    }

}