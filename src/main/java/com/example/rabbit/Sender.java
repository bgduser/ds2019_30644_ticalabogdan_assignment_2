package com.example.rabbit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.xml.crypto.Data;
import java.io.IOException;
import java.util.List;

@Service
public class Sender {

    private static final Logger log = LoggerFactory.getLogger(Sender.class);

    @Autowired
    private  RabbitTemplate rabbitTemplate;
    private DataReader dataReader=new DataReader();

    public Sender(final RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
    }

    @Scheduled(fixedDelay = 1000L)
    public void sendPracticalTip() throws IOException {
        Message msg = dataReader.computeMessage();
       // System.out.println(msg.toString());

        rabbitTemplate.convertAndSend(RabbitApplication.EXCHANGE_NAME, RabbitApplication.ROUTING_KEY, msg);
        log.info("Practical Tip sent");


    }
}