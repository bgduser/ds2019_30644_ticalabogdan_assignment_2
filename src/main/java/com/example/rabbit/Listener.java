package com.example.rabbit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Service;

import java.time.temporal.ChronoUnit;

@Service
public class Listener {

    private static final Logger log = LoggerFactory.getLogger(Listener.class);

    @RabbitListener(queues = RabbitApplication.DEFAULT_PARSING_QUEUE)
    public void consumeDefaultMessage(Message message) {

        if ( (message.getActivity().equals("Sleeping")) && (ChronoUnit.HOURS.between(message.getStartTime(), message.getEndTime()) > 4) )
        {
            System.out.println("Doarme prea mult");
        }
        else if ( (message.getActivity().equals("Leaving")) && (ChronoUnit.HOURS.between(message.getStartTime(), message.getEndTime()) > 4) ){
            System.out.println("Iese prea mult");
        }
        else if ( (message.getActivity().equals("Toileting")) && (ChronoUnit.HOURS.between(message.getStartTime(), message.getEndTime()) > 1) ){
            System.out.println("Sta la baie prea mult");
        }

        System.out.println("s--------------------------------" + message.toString());
    }
}